<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
//  api categoria
Route::get('/categoria', 'CategoriaController@index');
Route::post('/categoria', 'CategoriaController@store');
Route::post('/categoria/{id}', 'CategoriaController@update');
Route::delete('/deleteCategoria/{id}', 'CategoriaController@destroy');
Route::get('/getCategoriaSpecified/{id}', 'CategoriaController@edit');
Route::get('/getcategoriaEspesificaConRelacion', 'CategoriaController@categoriaEspesificaConRelacion');
//  api sub categoria
Route::get('/subCategoria', 'SubCategoriaController@index');
Route::post('/subCategoria', 'SubCategoriaController@store');
// api producto
Route::post('/producto', 'ProductoController@store');
Route::get('/producto', 'ProductoController@index');
Route::get('/producto/{id}', 'ProductoController@productoEspesifico');
Route::get('/productoConCategoria', 'SubCategoriaController@productoConCategoriaEspesifica');
Route::get('/categoriaconRelacion/{id}', 'SubCategoriaController@categoriaEspesificaConRelacion');
