import Vue from 'vue'
import Router from 'vue-router'

import dashboard from './components/categoria_component.vue'
import producto from './components/producto.vue'
import productoDetalle from './components/productoDetalle.vue'

Vue.use(Router)
// {path: '/', name: 'dashboard', component: dashboard}
export default new Router({
  routes: [
    {path: '/', name: 'dashboard', component: dashboard},
  	{path: '/producto', name: 'producto', component: producto},
  	{path: '/producto/:producto', name: 'producto-detalle', component: productoDetalle}
  ]
  })

