<?php

namespace App\Http\Controllers;

use App\Models\Categoria_hija;
use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SubCategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $SubCategoria = Categoria_hija::with('categoriaAsociada')->get();
        return Response($SubCategoria, 200);
    }
     public function productoConCategoriaEspesifica(){
        $Categoria = Categoria_hija::with('productoAsociadaACategoria')->get();
        return Response($Categoria, 200);
    }
    public function categoriaEspesificaConRelacion($id){
        $Categoria = Categoria_hija::find($id);
        $Categoria->productoAsociadaACategoria;
        return Response($Categoria, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (is_file($request->foto)) {
            $path = Storage::disk('public')->put('imagenes', $request->file('foto'));
        }
        $subCategoria = Categoria_hija::create([
            'nombre' => $request->nombre,
            'foto' => asset($path),

        ]);
        $categoria = Categoria::find($request->categoria);
        $categoria->SubCategoriaAsociadas()->attach($subCategoria);

        return $categoria->SubCategoriaAsociadas;

    }
}
