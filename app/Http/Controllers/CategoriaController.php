<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categoria;
use Illuminate\Support\Facades\Storage;

class CategoriaController extends Controller
{
    public function index(){
    	return Categoria::all();
    }
    public function categoriaEspesificaConRelacion(){
        $Categoria = Categoria::with('SubCategoriaAsociadas')->get();
        return Response($Categoria, 200);
    }
    public function store(Request $request){
    	if (is_file($request->foto)) {
    		$path = Storage::disk('public')->put('imagenes', $request->file('foto'));
    	}
    	$Categoria = Categoria::create([
    		'nombre' => $request->data,
    		'foto' => asset($path),
    	]);
    	return Response($Categoria, 200);
    }
    public function update(Request $request, $id)
    {
		if (is_file($request->foto)) {
			$path = Storage::disk('public')->put('imagenes', $request->file('foto'));
		}
	    $categoria = Categoria::findOrFail($id)->update([
	    	'nombre' => $request->data,
			'foto' => asset($path),
	    ]);
        return ;
    }
    public function edit($id){
    	$categoria = Categoria::findOrFail($id);
    	return Response($categoria, 200);
    }
    public function destroy($id){
    	$Categoria = Categoria::findOrFail($id);
        $Categoria->delete();
        return;
    }
}
