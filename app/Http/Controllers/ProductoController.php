<?php

namespace App\Http\Controllers;

use App\Models\producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Categoria_hija;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return producto::all();
    }
    public function productoEspesifico($id){
        return producto::findOrFail($id);
    }
   

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (is_file($request->foto_producto_principal)) {
            $path1 = Storage::disk('public')->put('imagenes', $request->file('foto_producto_principal'));
        }
        if (is_file($request->foto_producto_dos)) {
            $path2 = Storage::disk('public')->put('imagenes', $request->file('foto_producto_dos'));
        }
        if (is_file($request->foto_producto_tres)) {
            $path3 = Storage::disk('public')->put('imagenes', $request->file('foto_producto_tres'));
        }

        $producto = producto::create([

            'nombre' => $request->nombre_producto,   
            'descripcion' => $request->descripcion_producto,    
            'peso' => $request->peso_producto,   
            'precio' => $request->precio_producto,     
            'categoria' => $request->categoria_producto , 
            'foto1' =>  asset($path1),
            'foto2' =>  asset($path2),
            'foto3' =>  asset($path3)

        ]);

        $producto->categoriaAsociada()->attach($request->categoria_producto);

        return Response($producto, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(producto $producto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit(producto $producto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, producto $producto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy(producto $producto)
    {
        //
    }
}
