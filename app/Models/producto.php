<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class producto extends Model
{
    protected $guarded = [];
    protected $hidden = [
    	'created_at',
    	'updated_at'
    ];

	public function categoriaAsociada(){
    	return $this->belongsToMany('App\Models\Categoria_hija');
    }
     
}
