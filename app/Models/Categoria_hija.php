<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria_hija extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];

    public function categoriaAsociada(){
    	return $this->belongsToMany('App\Models\Categoria');
    }
    public function productoAsociadaACategoria(){
    	return $this->belongsToMany('App\Models\producto');
    }

}
