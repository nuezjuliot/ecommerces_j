<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $guarded = [];
    protected $hidden = ['created_at', 'updated_at'];

    public function SubCategoriaAsociadas(){
    	return $this->belongsToMany('App\Models\Categoria_hija');
    }
}
