<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriaCategoriaHijaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoria_categoria_hija', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('categoria_id')->unsigned()->nullable();
            $table->bigInteger('categoria_hija_id')->unsigned()->nullable();

            $table->foreign('categoria_id')->references('id')->on('categorias');
            $table->foreign('categoria_hija_id')->references('id')->on('categoria_hijas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoria_categoria_hija');
    }
}
