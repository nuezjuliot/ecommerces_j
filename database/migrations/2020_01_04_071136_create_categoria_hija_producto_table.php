<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriaHijaProductoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoria_hija_producto', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('producto_id')->unsigned()->nullable();
            $table->bigInteger('categoria_hija_id')->unsigned()->nullable();

            $table->foreign('producto_id')->references('id')->on('productos');
            $table->foreign('categoria_hija_id')->references('id')->on('categoria_hijas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categoria_hija_producto');
    }
}
